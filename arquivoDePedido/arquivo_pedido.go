package arquivoDePedido

import (
	"bufio"
	"os"
)

type ArquivoDePedido struct {
	Pedido Pedido `json:"Pedido"`
}

func GetStruct(fileHandle *os.File) (ArquivoDePedido, error) {
	fileScanner := bufio.NewScanner(fileHandle)

	arquivo := ArquivoDePedido{}
	err := arquivo.Pedido.ComposeStruct(fileScanner.Text())
	return arquivo, err
}
