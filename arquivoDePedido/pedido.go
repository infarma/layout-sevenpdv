package arquivoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Pedido struct {
	CodigoPDV                  int64   `json:"CodigoPDV"`
	CodigoProduto              int64   `json:"CodigoProduto"`
	CodigoAdministradora       string  `json:"CodigoAdministradora"`
	QuantidadeSolicitada       int32   `json:"QuantidadeSolicitada"`
	DescontoSolicitadoParaPDV  float32 `json:"DescontoSolicitadoParaPDV"`
	DescontoReposicaoParaCD    float32 `json:"DescontoReposicaoParaCD"`
	FormaProvavelReposicaoCD   string  `json:"FormaProvavelReposicaoCD"`
	CondicaoFaturamentoParaPDV int32   `json:"CondicaoFaturamentoParaPDV"`
	NumeroPedidoPDV            string  `json:"NumeroPedidoPDV"`
}

func (p *Pedido) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesPedido

	err = posicaoParaValor.ReturnByType(&p.CodigoPDV, "CodigoPDV")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CodigoAdministradora, "CodigoAdministradora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.QuantidadeSolicitada, "QuantidadeSolicitada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.DescontoSolicitadoParaPDV, "DescontoSolicitadoParaPDV")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.DescontoReposicaoParaCD, "DescontoReposicaoParaCD")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.FormaProvavelReposicaoCD, "FormaProvavelReposicaoCD")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.CondicaoFaturamentoParaPDV, "CondicaoFaturamentoParaPDV")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&p.NumeroPedidoPDV, "NumeroPedidoPDV")
	if err != nil {
		return err
	}

	return err
}

var PosicoesPedido = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoPDV":                  {0, 14, 0},
	"CodigoProduto":              {14, 27, 0},
	"CodigoAdministradora":       {27, 29, 0},
	"QuantidadeSolicitada":       {29, 33, 0},
	"DescontoSolicitadoParaPDV":  {33, 38, 2},
	"DescontoReposicaoParaCD":    {38, 43, 2},
	"FormaProvavelReposicaoCD":   {43, 44, 0},
	"CondicaoFaturamentoParaPDV": {44, 46, 0},
	"NumeroPedidoPDV":            {46, 61, 0},
}
