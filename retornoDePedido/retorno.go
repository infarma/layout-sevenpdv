package retornoDePedido

import "bitbucket.org/infarma/gerador-layouts-posicoes"

type Retorno struct {
	CodigoPDV                           int64   `json:"CodigoPDV"`
	CodigoProduto                       int64   `json:"CodigoProduto"`
	NumeroArquivoCentral                int32   `json:"NumeroArquivoCentral"`
	CampoControle                       string  `json:"CampoControle"`
	StatusFaturamento                   int32   `json:"StatusFaturamento"`
	QuantidadeTratada                   int32   `json:"QuantidadeTratada"`
	NumeroNotaFiscalEntregaDistribuidor int64   `json:"NumeroNotaFiscalEntregaDistribuidor"`
	DataNotaFiscalEntregaDistribuidor   int32   `json:"DataNotaFiscalEntregaDistribuidor"`
	DescontoConcedidoPDV                float32 `json:"DescontoConcedidoPDV"`
	CodigoAdministradora                string  `json:"CodigoAdministradora"`
	NumeroPedidoPDV                     string  `json:"NumeroPedidoPDV"`
}

func (r *Retorno) ComposeStruct(fileContents string) error {
	var err error

	var posicaoParaValor gerador_layouts_posicoes.PosicaoParaValor

	//Passo o conteúdo do arquivo
	posicaoParaValor.FileContents = fileContents

	//Passo as posicoes referentes a esse struct
	posicaoParaValor.Posicoes = PosicoesRetorno

	err = posicaoParaValor.ReturnByType(&r.CodigoPDV, "CodigoPDV")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoProduto, "CodigoProduto")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroArquivoCentral, "NumeroArquivoCentral")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CampoControle, "CampoControle")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.StatusFaturamento, "StatusFaturamento")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.QuantidadeTratada, "QuantidadeTratada")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroNotaFiscalEntregaDistribuidor, "NumeroNotaFiscalEntregaDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DataNotaFiscalEntregaDistribuidor, "DataNotaFiscalEntregaDistribuidor")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.DescontoConcedidoPDV, "DescontoConcedidoPDV")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.CodigoAdministradora, "CodigoAdministradora")
	if err != nil {
		return err
	}

	err = posicaoParaValor.ReturnByType(&r.NumeroPedidoPDV, "NumeroPedidoPDV")
	if err != nil {
		return err
	}

	return err
}

var PosicoesRetorno = map[string]gerador_layouts_posicoes.Posicao{
	"CodigoPDV":                           {0, 14, 0},
	"CodigoProduto":                       {14, 27, 0},
	"NumeroArquivoCentral":                {27, 33, 0},
	"CampoControle":                       {33, 34, 0},
	"StatusFaturamento":                   {34, 37, 0},
	"QuantidadeTratada":                   {37, 41, 0},
	"NumeroNotaFiscalEntregaDistribuidor": {41, 56, 0},
	"DataNotaFiscalEntregaDistribuidor":   {56, 64, 0},
	"DescontoConcedidoPDV":                {64, 69, 2},
	"CodigoAdministradora":                {69, 71, 0},
	"NumeroPedidoPDV":                     {71, 86, 0},
}
